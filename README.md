# Boletín 1

En este repositorio os encontraréis ejercicios del boletín 2 que se han realizado en clase de prácticas.

Quizás encontréis alguna pequeña modificación con respecto al enunciado.
Recordar que esas pequeñas modificaciones han sido realizadas para explicar cosas concretas que ocurren al usar el lenguaje Python.

# Ejercicio 01.

Plantee e implemente un programa que solicite un número entero por teclado y muestre por pantalla si es positivo o negativo.

### Solución

```python
if __name__ == "__main__":
    numero = int(input("Introduzca un número entero: "))
    if numero >= 0:
        print('El número', numero, 'es positivo.')
    else:
        print('El número', numero, 'es negativo.')
```

# Ejercicio 02.

Plantee e implemente un programa que solicite un número entero por teclado y muestre por pantalla si es par o impar.

### Solución

```python
if __name__ == "__main__":
    numero = int(input("Introduzca un número entero: "))
    if numero %2 == 0:
        print('El número', numero, 'es par.')
    else:
        print('El número', numero, 'es impar.')
```

# Ejercicio 03.

Plantee e implemente un programa que solicite por teclado dos números enteros y que muestre por pantalla un mensaje indicando si el primero es o no divisible por el segundo.

### Solución

```python
if __name__ == "__main__":
	n1 = int(input("Introduce un numero: "))
	n2 = int(input("Introduce otro numero: "))

	if n1 % n2 == 0:
		print(n1, "es divisible por", n2)
	else:
		print(n1, "no es divisible por", n2)

```

# Ejercicio 04.

Plantee e implemente un programa que solicite por teclado tres números enteros y muestre el mayor de ellos.

```
Primer Número: 123
Segundo Número: 1234
Tercer Número: 234

1234 es el mayor número de los introducidos.
```

### Solución

```python
if __name__ == "__main__":
	n1 = int(input("Primer numero: "))
	n2 = int(input("Segundo numero: "))
	n3 = int(input("Tercer numero: "))
	
	if n1 >= n2 and n1 >= n3:
		aux = n1
	elif n2 >= n1 and n2 >= n3:
		aux = n2
	else:
		aux = n3

	print(aux, "es el mayor numero de los introducidos.")

```

# Ejercicio 05.

Plantee e implemente un programa que solicitando tres números enteros los muestre ordenados de mayor a menor.

```
Primer Número: 123
Segundo Número: 1234
Tercer Número: 234

123<=234<=1234
```

### Solución

```python
if __name__ == "__main__":
	n1 = int(input("Primer numero: "))
	n2 = int(input("Segundo numero: "))
	n3 = int(input("Tercer numero: "))
	
	if n1 <= n2 and n2 <= n3:
		print(n1, "<=", n2, "<=", n3, sep='')
	elif n1 <= n3 and n3 <= n2:
		print(n1, "<=", n3, "<=", n2, sep='')
	elif n2 <= n1 and n1 <= n3:
                print(n2, "<=", n1, "<=", n3, sep='')
	elif n2 <= n3 and n3 <= n1:
                print(n2, "<=", n3, "<=", n1, sep='')
	elif n3 <= n1 and n1 <= n2:
                print(n3, "<=", n1, "<=", n2, sep='')
	else:
                print(n3, "<=", n2, "<=", n1, sep='')

```

# Ejercicio 06.

Implemente un programa que resuelva ecuaciones de segundo grado empleando sentencias que contengan elif para los casos:

- Reales y distintas
- Complejas conjugadas
- Raíz real doble.


### Solución

```python
import math

if __name__ == "__main__":
	print("Resolucion de una ecuacion de segundo grado.")
	a = float(input("Introduce la componente a: "))
	b = float(input("Introduce la componente b: "))
	c = float(input("Introduce la componente c: "))

	aux = b**2 - 4 * a * c

	if aux == 0:
		print("raiz real doble")
		print("\t", -b/(2*a) )
	elif aux > 0:
		print("reales y simples")
		print("\t", (-b + math.sqrt(aux))/(2*a) )
		print("\t", (-b - math.sqrt(aux))/(2*a) )
	else:
		print("complejas conjugadas")
		print("\t", -b/(2*a), " + ", math.sqrt(-aux)/(2*a), "i", sep="")
		print("\t", -b/(2*a), " - ", math.sqrt(-aux)/(2*a), "i", sep="")
```

# Ejercicio 07.

Plantee e implemente un programa que solicite al usuario una fecha y le muestre el número de días transcurridos desde el comienzo de año hasta esa fecha.

```
Escriba el año: 2015
Escriba el mes: 10
Escriba el día: 15

La fecha introducida corresponde al día 288 del año 2015
```

### Solución

```python
if __name__ == "__main__":
	year = int(input("Escriba el ano: "))
	month = int(input("Escriba el mes: "))
	day = int(input("Escriba el dia: "))

	leap_year = (year % 4 == 0) and (year % 100 != 0 or year % 400 == 0)
	if leap_year:
		feb = 29
	else:
		feb = 28	

	total = day
	if month == 12:
		total += feb + 6*31 + 4*30
	elif month == 11:
		total += feb + 6*31 + 3*30
	elif month == 10:
		total += feb + 5*31 + 3*30
	elif month == 9:
		total += feb + 5*31 + 2*30
	elif month == 8:
		total += feb + 4*31 + 2*30
	elif month == 7:
		total += feb + 3*31 + 2*30
	elif month == 6:
		total += feb + 3*31 + 1*30
	elif month == 5:
		total += feb + 2*31 + 1*30
	elif month == 4:
		total += feb + 2*31
	elif month == 3:
		total += feb + 31
	elif month == 2:
		total += feb

	print("La fecha introducida corresponde al dia", total, "del ano", year)  
```

# Ejercicio 08.

Plantee e implemente un programa que implemente el algoritmo necesario para calcular la edad de una persona suponiendo que se le pide al usuario que introduzca su fecha de nacimiento por teclado y a la salida muestra por pantalla su edad con respecto a la fecha actual. Tenga en cuenta el cumpleaños de la persona para realizar el cálculo.

### Solución

```python
from datetime import datetime

if __name__ == "__main__":
	now = datetime.now()

	current_year = now.year
	current_month = now.month
	current_day = now.day

	print("Fecha actual", current_day, "/", current_month, "/", current_year)

	year = int(input("Introduzca el año de su cumpleaños: "))
	month = int(input("Introduzca el mes de su cumpleaños: "))
	day = int(input("Introduzca el dia de su cumpleaños: "))

	aux = current_year - year;
	if month < current_month:
		aux -= 1;
	elif month == current_month and day < current_day:
		aux -= 1; 


	if aux < 0:
		print("La fecha introducida es invalida:", day, "/", month, "/", year)
		print("(fecha actual ", current_day, "/", current_month, "/", current_year),
	else:
		print("Tienes", aux, "años");



```

# Ejercicio 09.

Plantee e implemente un programa que solicite el salario anual bruto de una persona y muestre por pantalla la reducción del IRPF que se le aplicaría según los hijos menores de 18 años que tenga a su cargo, sabiendo que cada hijo (hasta 5) reduce un 10% el total del impuesto que le correspondería pagar. Es decir, si un trabajador gana 20000 € anuales y tiene a su cargo 5 hijos menores de 18 años, suponiendo un IRPF del 15% (debe ser constante en el programa) tendría que pagar 3000€ de impuesto anual, pero se le reduce un n_hijos*10% (50%) y por tanto pagaría únicamente 1500€ al año..

```
Introduzca su salario anual bruto (en euros): 20000
Introduzca el número de hijos mayores de 18 años a su cargo: 5

IRPF (15%): 3000
Reducción debida a hijos a cargo: 1500
Total anual a pagar: 1500
```

### Solución

```python

if __name__ == "__main__":
	IRPF = 0.15
	IRPF_PER_CHILD = 0.1
	MAX_CHILDREN = 5

	salary = float(input("Introduzca su salario anual bruto (en euros): "))
	children = int(input("Introduzca el numero de hijos menores de 18 años a su cargo: "))

	tmp = IRPF_PER_CHILD * (MAX_CHILDREN if children > MAX_CHILDREN else children)
	tmp_irpf = salary * IRPF

	print("IRFP (", IRPF * 100, "%): ", tmp_irpf, sep="")
	print("Reduccion debida a hijos a cargo:", tmp_irpf * tmp);
	print("Total anual a pagar:", tmp_irpf * (1 - tmp));
```

# Ejercicio 10.

Plantee e implemente un programa que solicite tres números enteros positivos y que muestre por pantalla el cociente y el resto de dividir el mayor de ellos por el menor.

```
Introduzca primer número: 34
Introduzca segundo número: 22
Introduzca tercer número: 67

67 divido | entre 22
          -----------
     R:1. C:3
```

NOTA: Se debe comprobar que el número menor es distinto de cero para evitar el error de la división por cero.

### Solución

```python

if __name__ == "__main__":
	n1 = int(input("Introduzca primer numero: "))
	n2 = int(input("Introduzca segundo numero: "))
	n3 = int(input("Introduzca tercer numero: "))

	if n1 >= n2 and n1 >= n3:
		high = n1
	elif n2 >= n1 and n2 >= n3:
		high = n2
	else:
		high = n3

	if n1 <= n2 and n1 <= n3:
		low = n1
	elif n2 <= n1 and n2 <= n3:
		low = n2
	else:
		low = n3

	if low != 0:
		print(high, "dividido entre", low)
		print("R:", high // low, "  C:", high % low, sep="")
	else:
		print("No se pueden realizar divisiones entre cero")

```

# Ejercicio 11.

Plantee e implemente un programa para clasificar triángulos en función de las longitudes de sus lados, que se solicitarán por teclado, de forma que se indique por pantalla si el triángulo es equilátero (todos los lados iguales), isósceles (dos lados iguales) o escaleno (todos los lados diferentes).

```
Introduzca la longitud del primer lado del triángulo (cm): 25
Introduzca la longitud del segundo lado del triángulo (cm): 35
Introduzca la longitud del tercer lado del triángulo (cm): 25

Es un triángulo isósceles (l1=l3=125cm)
```

### Solución

```python

if __name__ == "__main__":
	n1 = int(input("Introduzca la longitud del primer lado del triángulo (cm): "))
	n2 = int(input("Introduzca la longitud del segundo lado del triángulo (cm): "))
	n3 = int(input("Introduzca la longitud del tercer lado del triángulo (cm): "))


	if n1 == n2 and n2 == n3:
		print("Es un triángulo equilatero (", n1, ",", n2, ",", n3, ")", sep="")
	elif n1 == n2 or n2 == n3 or n1 == n3:
		print("Es un triángulo isosceles (", n1, ",", n2, ",", n3, ")", sep="")
	else:
		print("Es un triángulo escaleno (", n1, ",", n2, ",", n3, ")", sep="")
```

# Ejercicio 12.

Plantee e implemente un programa que solicite por teclado dos números enteros positivos (el primero de tres cifras y el segundo de 1) y que muestre por pantalla su multiplicación en el formado que se indica en el ejemplo. Se debe comprobar que el primer número tiene efectivamente 3 cifras y es positivo y que el segundo número es también positivo y tiene una única cifra. En caso de que no se cumplan estas condiciones, el programa debe alertar al usuario con un mensaje apropiado (Ej. “El primer número no tiene 3 cifras!!!).

```
Introduzca un número positivo de 3 cifras: 125
Introduzca un número positivo de 1 cifra: 3

125
 X3
---
375
```

### Solución

```python

if __name__ == "__main__":
	n1 = int(input("Introduzca un número positivo de 3 cifras: "))
	n2 = int(input("Introduzca un número positivo de 1 cifra:  "))

	if n1 < 100 or n1 > 999:
		print("El primer numero no tiene 3 cifras!!!")
	elif n2 < 0 or n2 > 9:
		print("El segundo numero no tiene 1 cifra!!!")
	else:
		print(n1, "\nx ", n2, "\n---\n", n1 * n2, sep="")

```

# Ejercicio 13.

Plantee e implemente un programa que presente el menú del ejemplo y que, según el carácter introducido por el usuario, escriba en pantalla bebé, adolescente, mujer, hombre.

```
B. Bebé
A. Adolescente
M. Mujer
H. Hombre
```

### Solución

```python

if __name__ == "__main__":
	print("B. Bebe\nA. Adolescente\nM. Mujer\nH. Hombre")
	s = input("Opcion: ")

	if s == "B" or s == "b":
		print("Has seleccionado: Bebe")
	elif s == "A" or s == "a":
		print("Has seleccionado: Adolescente")
	elif s == "M" or s == "m":
		print("Has seleccionado: Mujer")
	elif s == "H" or s == "h":
		print("Has seleccionado: Hombre")
	else:
		print("Opcion no valida")
		
```

# Ejercicio 14.

Plantee e implemente un programa que admita el nombre y la edad de una persona y muestre por pantalla si la persona es joven, adulta o anciana según los siguientes criterios:

- Si tiene menos de 32 años es joven
- Si tiene 32 o más años y menos de 75 es adulta 
- Si tiene 75 o más años es anciana

```
Nombre: Pedro
Edad: 25

Pedro es JOVEN (< 32)
```

### Solución

```python

if __name__ == "__main__":
	name = input("Nombre: ")
	age = int(input("Edad: "))

	if age < 32:
		print(name, "es JOVEN (<32)")
	elif age < 75:
		print(name, "es ADULTO (<75)")
	else:
		print(name, "es ANCIANA (>=75)")
```

# Ejercicio 15.

Plantee e implemente un programa que admita un carácter (introducido por teclado) y muestre por pantalla si es una vocal, una consonante, un dígito o un carácter especial.

### Solución

```python
if __name__ == "__main__":
	
	c = input("Introduce un caracter: ")

	ascii = ord(c)

	if ascii >= ord('0') and ascii <= ord('9'):
		print(c, "es un numero")
	elif (ascii >= ord('A') and ascii <= ord('Z')) or (ascii >= ord('a') and ascii <= ord('z')):
		if c == 'A' or c == 'E' or c == 'I' or c == 'O' or c == 'U' or \
                   c == 'a' or c == 'e' or c == 'i' or c == 'o' or c == 'u':
			print(c , "es una vocal")
		else:
			print(c, "es una consonante")
	else:
		print(c, "es un caracter especial")
```

# Ejercicio 16.

Plantee e implemente un programa que adivine un número del 1 al 10 conociendo su paridad y el resto de dividirlo por cinco.

```
Piense en un número del 1 al 10.
¿El número que ha pensado es impar (1) o par (2)? 2
¿Cuál es el resto de dividir el número que ha pensado entre 5? 3

El número que ha pensado es el 8.
```

### Solución

```python

if __name__ == "__main__":
	print("Piense en un número del 1 al 10.")
	aux = int(input("¿El número que ha pensado es impar (1) o par (2)? "))
	mod = int(input("¿Cuál es el resto de dividir el número que ha pensado entre 5? "))

	if aux == 2:
		if mod == 0:
			number = 10
		else:
			number = (0 if mod % 2 == 0 else 5) + mod;
	else:
		number = (5 if mod % 2 == 0 else 0) + mod

	print("El número en el que has pensado es el", number)

```

# Ejercicio 17.

Plantee e implemente un programa para calcular los días que tiene un mes. Tenga en cuenta que si el mes introducido por el usuario es Febrero, el programa deberá solicitar el año del que se trata para poder determinar si es bisiesto o no.

```
Introduzca el nombre del mes del que quiere calcular los días: Julio
El mes de Julio tiene 31 días

Introduzca el nombre del mes del que quiere calcular los días: Febrero
Introduzca el año: 2018
El mes de febrero del año 2018 tiene 28 días
```

### Solución

```python

if __name__ == "__main__":

	month = int(input("Introduzca el numero del mes del que quiere calcular los días [1-12]: "))

	if month == 2:
		year = int(input("Introduzca el año: "))
		days = 28 if (year % 4 == 0) and (year % 100 != 0 or year % 400 == 0) else 27;
		print("El mes", month, "del ano", year, "tiene", days, "dias");
	else:
		aux = month if month < 8 else (month % 8) + 1;
		days = 30 if aux % 2 == 0 else 31;
		print("El mes", month, "tiene", days, " dias");

	
	
```

# Ejercicio 18.

Plantee e implemente un programa que solicite a un usuario su nombre, su peso (en kilos) y la altura (en centímetros) de una persona por teclado y calcule su índice de masa corporal , indicando por pantalla si la persona está por debajo de su peso de salud (IMC < 18.5), normal (18.5<=IMC<25), con sobrepreso de grado I (25 <= IMC < 27), sobrepeso de grado II (27<=IMC<30) u obesa (IMC>30).

```
Introduzca su nombre: Pablo
Introduzca su altura (en centímetros): 178
Introduzca su peso (en kilos): 73

Pablo, según el índice de masa corporal (IMC=23.04), tiene un peso normal
para su estatura.
```

### Solución

```python

if __name__ == "__main__":

	name = input("Introduzca su nombre: ")
	height = int(input("Introduzca su altura (en cm): "))
	weight = int(input("Introduzca su peso (en kg): "))

	imc = weight / pow(height/100, 2)

	if imc < 18.5:
		s = "está por debajo de su peso de salud"
	elif imc < 25:
		s = "tiene un peso normal"
	elif imc < 27:
		s = "tiene sobrepeso de grado I"
	elif imc < 30:
		s = "tiene sobrepeso de grado II"
	else:
		s = "tiene obesidad"


	print(name, ", según su índice de masa corporal (IMC=", imc, "), ", s, sep="")

	
```

# Ejercicio 19.

Plantee e implemente un programa que calcule la nota media de 6 asignaturas de un curso académico a partir de las calificaciones introducidas individualmente por teclado. Debe mostrar la calificación media también en formato:

- Sobresaliente (9-10)
- Notable (7-8.9)
- Aprobado (5-6.9)
- Suspenso (0-4.9)
 
En el caso de que el alumno no se haya presentado a la asignatura se introducirá un -1 como calificación.

```
Calificación de la asignatura 1 (-1 si no se presentó): 4.7
Calificación de la asignatura 2 (-1 si no se presentó): 5.1
Calificación de la asignatura 3 (-1 si no se presentó): 7.4
Calificación de la asignatura 4 (-1 si no se presentó): -1
Calificación de la asignatura 5 (-1 si no se presentó): 3.7
Calificación de la asignatura 6 (-1 si no se presentó): 8.5

Nota media: 5.8 (APROBADO)
```

NOTA: compruebe que las calificaciones introducidas por teclado son correctas, es decir están entre 0 y 10 puntos o -1 para las no presentadas.


### Solución

```python

if __name__ == "__main__":

	aux = 0
	sum = 0
	for cnt in range(6):
		grade = -10
		while grade != -1 and (grade < 0 or grade > 10):
			grade = float(input("Calificación de la asignatura " + str(cnt + 1) + " (-1 si no se presentó): "))
		
		if grade != -1:
			aux += 1
			sum += grade

	if aux == 0:
		print("No se ha presentado a ninguna asignatura")
	else:
		avg = sum/aux
		if avg >= 9:
			s = "(SOBRESALIENTE)"
		elif avg >= 7:
			s = "(NOTABLE)"
		elif avg >= 5:
			s = "(APROBADO)"
		else:
			s = "(SUSPENSO)"

		print("Nota media:", avg, s)
```

# Ejercicio 20.

Plantee e implemente un programa que sume los primeros 100 números enteros pares por una parte y los impares por otra.

### Solución

```python
if __name__ == "__main__":
	even = 0
	odd = 0

	for cnt in range(200):
		if cnt % 2 == 0:
			even += cnt
		else:
			odd += cnt

	print("La suma de los primeros 100 numeros pares es", even)
	print("La suma de los primeros 100 numeros impares es", odd)

```

# Ejercicio 21.

Plantee e implemente un programa que esté solicitando números constantemente hasta que el usuario introduzca un número comprendido entre 20 y 30 ambos inclusive.

### Solución

```python
if __name__ == "__main__":
	MIN_VALUE = 20
	MAX_VALUE = 30

	aux = MIN_VALUE - 1
	while aux < MIN_VALUE or aux > MAX_VALUE:
		aux = int(input("Introduce un numero comprendido entre " + str(MIN_VALUE) + " y " + str(MAX_VALUE) + ": " ))

	print("El numero introducido ha sido", aux)
```

# Ejercicio 22.

Plante e implemente un programa que muestra por pantalla la tabla de multiplicar del 7 en un formato similar al siguiente:

```
7 x 0 = 0
7 x 1 = 7
7 x 2 = 14
7 x 3 = 21
7 x 4 = 28
7 x 5 = 35
7 x 6 = 42
7 x 7 = 49
7 x 8 = 56
7 x 9 = 63
7 x 10 = 70
```

### Solución

```python

if __name__ == "__main__":
	VALUE= 7

	for cnt in range(11):
		print(VALUE, "x", cnt, "=", VALUE * cnt)

```

# Ejercicio 23.

Plantee e implemente un programa que muestre por pantalla la tabla de multiplicar (0-10) de un número entero positivo introducido por el teclado. Muestre la salida en un formato similar al indicado en el ejercicio anterior.

### Solución

```python

if __name__ == "__main__":
	
	aux = -1
	while aux < 0:
		aux = int(input("Introduce un numero entero positivo: "))

	for cnt in range(11):
		print(aux, "x", cnt, "=", aux * cnt)

```

# Ejercicio 24.

Plante e implemente un programa que sume los primeros 100 números enteros. Realízalo con los tipos de bucles que conozcas.

### Solución

```python

if __name__ == "__main__":

	print("BUCLE FOR")

	sum = 0
	for cnt in range(100):
		sum += cnt

	print("El resultado es", sum, "\n")

	print("BUCLE WHILE")
	sum = 0
	cnt = 0
	while cnt < 100:
		sum += cnt
		cnt += 1

	print("El resultado es", sum)

```

# Ejercicio 25.

Plantee e implemente un programa que acepte valores enteros como entrada (por teclado) y que muestre por pantalla la suma de los valores introducidos. El programa terminará cuando el usuario introduzca un 0, tal y como se muestra en el ejemplo siguiente:

```Introduzca los números enteros de los que desee calcular la suma:
2
3
15
7
0
	La suma de los números introducidos es 27
```

### Solución

```python
if __name__ == "__main__":

	print("Introduza los números enteros de los que desee calcular la suma:")

	tmp = -1
	total = 0
	while tmp != 0:
		tmp = int(input(""))
		total += tmp

	print("La suma de los números introducidos es", total)

```

# Ejercicio 26.

Plantee e implemente un programa que acepte valores reales como entrada (por teclado) y que muestre por pantalla la media aritmética de los valores introducidos. El programa terminará cuando el usuario introduzca un 0.

### Solución

```python
if __name__ == "__main__":

	print("Introduza los números decimales de los que desee calcular la media:\n(0 parar finalizar)")

	tmp = -1
	total = 0
	cnt = 0
	while tmp != 0:
		tmp = float(input(""))
		if tmp != 0:
			total += tmp
			cnt += 1

	if cnt == 0:
		print("No has introducido valores")
	else:
		print("La media de los números introducidos es", total/cnt)

```

# Ejercicio 27.

Plantee e implemente un programa que reciba números enteros por teclado y al terminar indique cuánto vale la suma de los números pares y los impares por separado. La entrada de números terminará cuando se introduzca un cero.


### Solución

```python
if __name__ == "__main__":

	print("Introduza los números enteros de los que desee calcular la suma:")

	tmp = -1
	even = 0
	odd = 0
	while tmp != 0:
		tmp = int(input(""))
		if tmp % 2 == 0:
			even += tmp
		else:
			odd += tmp

	print("La suma de los números pares introducidos es", even)
	print("La suma de los números impares introducidos es", odd)

```

# Ejercicio 28.

Plantee e implemente un programa que muestre por pantalla una lista de los años bisiestos del siglo XIX. Haga que el programa no muestre la lista de los años bisiestos del siglo XX hasta que el usuario lo indique.

### Solución

```python

if __name__ == "__main__":

	cnt = 1801
	while cnt < 2001:
		if cnt == 1901:
			input("")

		if cnt % 4 == 0 and (cnt % 100 != 0 or cnt % 400 == 0):
			print(cnt)

		cnt += 1

```

# Ejercicio 29.

Plantee e implemente un programa que calcule la potencia n-ésima de un número entero introducido por el teclado para valores no negativos de n.

### Solución

```python

if __name__ == "__main__":
	base = float(input("Introduzca la base: "))
	n = int(input("Introduzca el exponente: "))

	if n < 0:
		print('El exponente no debería ser un numero negativo')
	else:
		total = 1
		for cnt in range(n):
			total *= base

		print("El resultado es ", total)
```

# Ejercicio 30.

Plantee e implemente un programa que calcule el factorial de un número entero positivo introducido por teclado.

### Solución

```python

if __name__ == "__main__":
	n = int(input("Introduce un valor para calcular su factorial:"))

	if n < 0:
		print("Solo se pueden introducir números positivos")
	else:
		total = 1
		cnt = 1
		while cnt <= n:
			total *= cnt
			cnt += 1

		print(n, "! = ", total, sep="")
			
```

# Ejercicio 32.

Plantee e implemente un programa que muestre un menú y dependiendo del carácter introducido, muestra por pantalla lo indicado y a continuación vuelva a mostrar el menú.

i – Muestra por pantalla: “En ingles good morning es buenos días”

g – Muestra por pantalla: “En gallego bos días es buenos días”

f – Muestra por pantalla: “En francés bonjur es buenos días”

a – Finaliza el programa


### Solución

```python
if __name__ == "__main__":
	
	s = ""
	while s != "a":
		print("i - INGLES\ng - Gallego\nf - Frances\na - Finaliza el programa")
		s = input("opcion: ")

		if s == "i":
			print("En ingles good morning es buenos días")
		elif s == "g":
			print("En gallego bos días es buenos días")
		elif s == "f":
			print("En francés bonjur es buenos días")
		elif s != "a":
			print("Opcion incorrecta")


	print("Fin de programa")

```

# Ejercicio 37.
Plantee e implemente un programa que muestre por pantalla los primeros 1000 números enteros que cumplen la siguiente condición: x<sup>2</sup> + y<sup>2</sup> = z<sup>2</sup>

### Solución

```python
num_resultados = 0
max_resultados = 1000
z = 1

while num_resultados != max_resultados:
    for x in range(1, z):
        for y in range(1, z):
            if x**2 + y**2 == z**2:
              num_resultados+=1
              print("{}: x={}, y={}, z={}".format(num_resultados, x, y, z))
            if num_resultados == max_resultados: break
        if num_resultados == max_resultados: break
    z += 1
	
```

# Ejercicio 39.

Plantee e implemente un programa que proporcione un valor aproximado del número e a partir del siguiente desarrollo de Taylor con error menor de 0,0001:

e<sup>x</sup> = 1 + x/1! + x<sup>2</sup>/2! + x<sup>3</sup>/3! + ... + x<sup>n</sup>/n!

### Solución

```python
import math


if __name__ == "__main__":
	
	x = float(input("Introduce el valor de x para calcular e^x: "))

	e_x = math.exp(x)
	total = 1.0
	error = math.fabs(total - e_x)
	num = 1
	den = 1
	cnt = 0
	while error > 0.0001:
		cnt += 1
		num *= x
		den *= cnt

		total += num/den
		error = math.fabs(total - e_x)

	print("El valor de e^", x, " es ", total, "(con un error de ", error, ")", sep = "")
```

# Ejercicio 40.

Plantee e implemente un programa que muestre por pantalla la tabla de multiplicación en el siguiente formato:
```
	1	2	3	4	5	6	7	8	9
1	1
2	2	4
3	3	6	9
4	4	8	12	16
5	5	10	15	20	25
6	6	12	18	24	30	36
7	7	14	21	28	35	42	49
8	8	16	24	32	40	48	56	64
9	9	18	27	36	45	54	63	72	81
```

### Solución

```python
if __name__ == "__main__":
	
	# header
	for cnt in range(9):
		print("\t", cnt+1, sep="", end="")
	print("")

	# body
	row = 1
	while row < 10:
		print(row, end="")
		col = 1
		while col <= row:
			print("\t", row * col, sep="", end="")
			col += 1
		
		print("")
		row += 1
	

```

# Ejercicio 41.

Plantee e implemente un programa para obtener las primeras n filas del triángulo de Floyd.

### Solución

```python
if __name__ == "__main__":
	n = int(input("Introduce el numero de filas para mostrar el triangulo de Floyd: "))

	cnt = 1
	for row in range(n + 1):
		for col in range(row):
			print(cnt, end="\t")
			cnt += 1
		print("")


```

# Ejercicio 42.

Plantee e implemente un programa para obtener elementos del triángulo de Floyd hasta un número dado.

### Solución

```python
if __name__ == "__main__":
	n = int(input("Introduce el numero al que deseas llegar en el triangulo de Floyd: "))

	cnt = 1
	row = 1
	while cnt <= n:
		col = 1
		while col <= row and cnt <= n:
			print(cnt, end="\t")
			col += 1
			cnt += 1
		print("")
		row += 1

```
