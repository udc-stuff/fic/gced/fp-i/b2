if __name__ == "__main__":
    numero = int(input("Introduzca un número entero: "))
    if numero >= 0:
        print('El número', numero, 'es positivo.')
    else:
        print('El número', numero, 'es negativo.')
