import math

if __name__ == "__main__":
	print("Resolucion de una ecuacion de segundo grado.")
	a = float(input("Introduce la componente a: "))
	b = float(input("Introduce la componente b: "))
	c = float(input("Introduce la componente c: "))

	aux = b**2 - 4 * a * c

	if aux == 0:
		print("raiz real doble")
		print("\t", -b/(2*a) )
	elif aux > 0:
		print("reales y simples")
		print("\t", (-b + math.sqrt(aux))/(2*a) )
		print("\t", (-b - math.sqrt(aux))/(2*a) )
	else:
		print("complejas conjugadas")
		print("\t", -b/(2*a), " + ", math.sqrt(-aux)/(2*a), "i", sep="")
		print("\t", -b/(2*a), " - ", math.sqrt(-aux)/(2*a), "i", sep="")
