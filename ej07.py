

if __name__ == "__main__":
	year = int(input("Escriba el ano: "))
	month = int(input("Escriba el mes: "))
	day = int(input("Escriba el dia: "))

	leap_year = (year % 4 == 0) and (year % 100 != 0 or year % 400 == 0)
	if leap_year:
		feb = 29
	else:
		feb = 28	

	total = day
	if month == 12:
		total += feb + 6*31 + 4*30
	elif month == 11:
		total += feb + 6*31 + 3*30
	elif month == 10:
		total += feb + 5*31 + 3*30
	elif month == 9:
		total += feb + 5*31 + 2*30
	elif month == 8:
		total += feb + 4*31 + 2*30
	elif month == 7:
		total += feb + 3*31 + 2*30
	elif month == 6:
		total += feb + 3*31 + 1*30
	elif month == 5:
		total += feb + 2*31 + 1*30
	elif month == 4:
		total += feb + 2*31
	elif month == 3:
		total += feb + 31
	elif month == 2:
		total += feb

	print("La fecha introducida corresponde al dia", total, "del ano", year)  
