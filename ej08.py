from datetime import datetime

if __name__ == "__main__":
	now = datetime.now()

	current_year = now.year
	current_month = now.month
	current_day = now.day

	print("Fecha actual", current_day, "/", current_month, "/", current_year)

	year = int(input("Introduzca el año de su cumpleaños: "))
	month = int(input("Introduzca el mes de su cumpleaños: "))
	day = int(input("Introduzca el dia de su cumpleaños: "))

	aux = current_year - year;
	if month > current_month:
		aux -= 1;
	elif month == current_month and day > current_day:
		aux -= 1; 


	if aux < 0:
		print("La fecha introducida es invalida:", day, "/", month, "/", year)
		print("(fecha actual ", current_day, "/", current_month, "/", current_year),
	else:
		print("Tienes", aux, "años");



