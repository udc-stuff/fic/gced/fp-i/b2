


if __name__ == "__main__":
	IRPF = 0.15
	IRPF_PER_CHILD = 0.1
	MAX_CHILDREN = 5

	salary = float(input("Introduzca su salario anual bruto (en euros): "))
	children = int(input("Introduzca el numero de hijos menores de 18 años a su cargo: "))

	tmp = IRPF_PER_CHILD * (MAX_CHILDREN if children > MAX_CHILDREN else children)
	tmp_irpf = salary * IRPF

	print("IRFP (", IRPF * 100, "%): ", tmp_irpf, sep="")
	print("Reduccion debida a hijos a cargo:", tmp_irpf * tmp);
	print("Total anual a pagar:", tmp_irpf * (1 - tmp));
