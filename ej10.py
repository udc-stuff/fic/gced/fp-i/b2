


if __name__ == "__main__":
	n1 = int(input("Introduzca primer numero: "))
	n2 = int(input("Introduzca segundo numero: "))
	n3 = int(input("Introduzca tercer numero: "))

	if n1 >= n2 and n1 >= n3:
		high = n1
	elif n2 >= n1 and n2 >= n3:
		high = n2
	else:
		high = n3

	if n1 <= n2 and n1 <= n3:
		low = n1
	elif n2 <= n1 and n2 <= n3:
		low = n2
	else:
		low = n3

	if low != 0:
		print(high, "dividido entre", low)
		print("C:", high // low, "  R:", high % low, sep="")
	else:
		print("No se pueden realizar divisiones entre cero")

