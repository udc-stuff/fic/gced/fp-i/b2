


if __name__ == "__main__":
	n1 = int(input("Introduzca la longitud del primer lado del triángulo (cm): "))
	n2 = int(input("Introduzca la longitud del segundo lado del triángulo (cm): "))
	n3 = int(input("Introduzca la longitud del tercer lado del triángulo (cm): "))


	if n1 == n2 and n2 == n3:
		print("Es un triángulo equilatero (", n1, ",", n2, ",", n3, ")", sep="")
	elif n1 == n2 or n2 == n3 or n1 == n3:
		print("Es un triángulo isosceles (", n1, ",", n2, ",", n3, ")", sep="")
	else:
		print("Es un triángulo escaleno (", n1, ",", n2, ",", n3, ")", sep="")
