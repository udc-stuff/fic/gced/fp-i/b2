


if __name__ == "__main__":
	n1 = int(input("Introduzca un número positivo de 3 cifras: "))
	n2 = int(input("Introduzca un número positivo de 1 cifra:  "))

	if n1 < 100 or n1 > 999:
		print("El primer numero no tiene 3 cifras!!!")
	elif n2 < 0 or n2 > 9:
		print("El segundo numero no tiene 1 cifra!!!")
	else:
		print(n1, "\nx ", n2, "\n---\n", n1 * n2, sep="")

