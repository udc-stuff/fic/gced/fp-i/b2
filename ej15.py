

if __name__ == "__main__":
	
	c = input("Introduce un caracter: ")

	ascii = ord(c)

	if ascii >= ord('0') and ascii <= ord('9'):
		print(c, "es un numero")
	elif (ascii >= ord('A') and ascii <= ord('Z')) or (ascii >= ord('a') and ascii <= ord('z')):
		if c == 'A' or c == 'E' or c == 'I' or c == 'O' or c == 'U' or \
                   c == 'a' or c == 'e' or c == 'i' or c == 'o' or c == 'u':
			print(c , "es una vocal")
		else:
			print(c, "es una consonante")
	else:
		print(c, "es un caracter especial")
