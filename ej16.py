


if __name__ == "__main__":
	print("Piense en un número del 1 al 10.")
	aux = int(input("¿El número que ha pensado es impar (1) o par (2)? "))
	mod = int(input("¿Cuál es el resto de dividir el número que ha pensado entre 5? "))

	if aux == 2:
		if mod == 0:
			number = 10
		else:
			number = (0 if mod % 2 == 0 else 5) + mod;
	else:
		number = (5 if mod % 2 == 0 else 0) + mod

	print("El número en el que has pensado es el", number)

