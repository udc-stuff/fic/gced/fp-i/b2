


if __name__ == "__main__":

	month = int(input("Introduzca el numero del mes del que quiere calcular los días [1-12]: "))

	if month == 2:
		year = int(input("Introduzca el año: "))
		days = 28 if (year % 4 == 0) and (year % 100 != 0 or year % 400 == 0) else 27;
		print("El mes", month, "del ano", year, "tiene", days, "dias");
	else:
		aux = month if month < 8 else (month % 8) + 1;
		days = 30 if aux % 2 == 0 else 31;
		print("El mes", month, "tiene", days, " dias");

	
	
