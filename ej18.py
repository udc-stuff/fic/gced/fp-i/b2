


if __name__ == "__main__":

	name = input("Introduzca su nombre: ")
	height = int(input("Introduzca su altura (en cm): "))
	weight = int(input("Introduzca su peso (en kg): "))

	imc = weight / pow(height/100, 2)

	if imc < 18.5:
		s = "está por debajo de su peso de salud"
	elif imc < 25:
		s = "tiene un peso normal"
	elif imc < 27:
		s = "tiene sobrepeso de grado I"
	elif imc < 30:
		s = "tiene sobrepeso de grado II"
	else:
		s = "tiene obesidad"


	print(name, ", según su índice de masa corporal (IMC=", imc, "), ", s, sep="")

	
