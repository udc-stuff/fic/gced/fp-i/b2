


if __name__ == "__main__":

	aux = 0
	sum = 0
	for cnt in range(6):
		grade = -10
		while grade != -1 and (grade < 0 or grade > 10):
			grade = float(input("Calificación de la asignatura " + str(cnt + 1) + " (-1 si no se presentó): "))
		
		if grade != -1:
			aux += 1
			sum += grade

	if aux == 0:
		print("No se ha presentado a ninguna asignatura")
	else:
		avg = sum/aux
		if avg >= 9:
			s = "(SOBRESALIENTE)"
		elif avg >= 7:
			s = "(NOTABLE)"
		elif avg >= 5:
			s = "(APROBADO)"
		else:
			s = "(SUSPENSO)"

		print("Nota media:", avg, s)
