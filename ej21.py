

if __name__ == "__main__":
	MIN_VALUE = 20
	MAX_VALUE = 30

	aux = MIN_VALUE - 1
	while aux < MIN_VALUE or aux > MAX_VALUE:
		aux = int(input("Introduce un numero comprendido entre " + str(MIN_VALUE) + " y " + str(MAX_VALUE) + ": " ))

	print("El numero introducido ha sido", aux)
