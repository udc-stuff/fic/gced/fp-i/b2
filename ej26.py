

if __name__ == "__main__":

	print("Introduza los números decimales de los que desee calcular la media:\n(0 parar finalizar)")

	tmp = -1
	total = 0
	cnt = 0
	while tmp != 0:
		tmp = float(input(""))
		if tmp != 0:
			total += tmp
			cnt += 1

	if cnt == 0:
		print("No has introducido valores")
	else:
		print("La media de los números introducidos es", total/cnt)

