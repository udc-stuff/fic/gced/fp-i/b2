

if __name__ == "__main__":

	print("Introduza los números enteros de los que desee calcular la suma:")

	tmp = -1
	even = 0
	odd = 0
	while tmp != 0:
		tmp = int(input(""))
		if tmp % 2 == 0:
			even += tmp
		else:
			odd += tmp

	print("La suma de los números pares introducidos es", even)
	print("La suma de los números impares introducidos es", odd)

