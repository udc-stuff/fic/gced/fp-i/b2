


if __name__ == "__main__":
	base = float(input("Introduzca la base: "))
	n = int(input("Introduzca el exponente: "))

	if n < 0:
		print('El exponente no debería ser un numero negativo')
	else:
		total = 1
		for cnt in range(n):
			total *= base

		print("El resultado es ", total)
