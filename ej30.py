


if __name__ == "__main__":
	n = int(input("Introduce un valor para calcular su factorial:"))

	if n < 0:
		print("Solo se pueden introducir números positivos")
	else:
		total = 1
		cnt = 1
		while cnt <= n:
			total *= cnt
			cnt += 1

		print(n, "! = ", total, sep="")
			
