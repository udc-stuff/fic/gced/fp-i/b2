#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 13:31:59 2021

@author: alberto
"""

num_resultados = 0
max_resultados = 100
z = 1

while num_resultados != max_resultados:
    for x in range(1, z):
        for y in range(1, z):
            if x**2 + y**2 == z**2:
              num_resultados+=1
              print(f"{num_resultados}: x={x}, y={y}, z={z}")
            if num_resultados == max_resultados: break
        if num_resultados == max_resultados: break
    z += 1
