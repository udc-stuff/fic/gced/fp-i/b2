import math


if __name__ == "__main__":
	
	x = float(input("Introduce el valor de x para calcular e^x: "))

	e_x = math.exp(x)
	total = 1.0
	error = math.fabs(total - e_x)
	num = 1
	den = 1
	cnt = 0
	while error > 0.0001:
		cnt += 1
		num *= x
		den *= cnt

		total += num/den
		error = math.fabs(total - e_x)

	print("El valor de e^", x, " es ", total, "(con un error de ", error, ")", sep = "")
