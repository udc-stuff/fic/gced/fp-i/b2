

if __name__ == "__main__":
	
	# header
	for cnt in range(9):
		print("\t", cnt+1, sep="", end="")
	print("")

	# body
	row = 1
	while row < 10:
		print(row, end="")
		col = 1
		while col <= row:
			print("\t", row * col, sep="", end="")
			col += 1
		
		print("")
		row += 1
	

