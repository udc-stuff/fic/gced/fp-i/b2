

if __name__ == "__main__":
	n = int(input("Introduce el numero al que deseas llegar en el triangulo de Floyd: "))

	cnt = 1
	row = 1
	while cnt <= n:
		col = 1
		while col <= row and cnt <= n:
			print(cnt, end="\t")
			col += 1
			cnt += 1
		print("")
		row += 1

